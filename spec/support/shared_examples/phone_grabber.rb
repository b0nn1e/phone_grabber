shared_examples_for "phone grabber" do
  let(:klass) { described_class }

  describe '#domain' do
    let(:subject) { klass.domain }
    it 'must be start with http/https' do
      expect(subject).to match /^(http:\/\/|https:\/\/)/
    end
    it 'must end at /' do
      expect(subject.last).to eq '/'
    end
  end

  describe '#brands_url' do
    let(:subject) { klass.brands_url }

    it 'OpenURI can read url/file content' do
      expect { open(subject) }.to_not raise_error
    end

    it 'OpenURI can read url/file content' do
      expect(open(subject).read).to_not be_nil
    end
  end

  describe '#brands' do
    let(:subject) { klass.brands }

    it 'return array' do
      expect(subject.class).to eq(Array)
    end

    it 'return not empty array' do
      expect(subject).not_to eq(0)
    end

    it 'any hashes values can`t be empty' do
      subject.each do |hash|
        expect(hash[:uri]).not_to be_nil
        expect(hash[:name]).not_to be_nil
      end
    end

    it 'any hash uri exclude domain' do
      subject.each do |hash|
        expect(hash[:uri]).not_to include(klass.domain)
      end
    end
  end
 end