require 'rails_helper'
require 'open-uri'

RSpec.describe Gsmarena do
  it_behaves_like 'phone grabber'

  describe '#parse_brands_list' do

  end

  describe '#parse_models_list' do

  end

  describe '#parse_model_info' do
    let(:html) { open("#{Rails.root}/spec/support/fixtures/gsmarena-item.html").read }
    let(:subject) { Gsmarena.parse_model_info(html) }

    it 'return hash' do
      expect(subject.class).to eq(Array)
    end

    it 'results has 10 items' do
      expect(subject.count).to eq 10
    end

  end

  describe '#pagination_links' do
    let(:html) { open("#{Rails.root}/spec/support/fixtures/gsmarena-category_with_pagination.html").read }
    let(:subject) { Gsmarena.pagination_links(html) }

    it 'return array' do
      expect(subject.class).to eq(Array)
    end

    it 'total 6 pages' do
      expect(subject.count).to eq 3
    end
  end

  describe '#parse_model_info' do

  end

  describe '#find' do

  end
end
