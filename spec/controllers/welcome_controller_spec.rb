require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  describe 'GET #index' do

    before { get :index }

    it 'render index view' do
      expect(response).to render_template(:index)
    end

    it 'return status 200' do
      expect(response.status).to eq(200)
    end
  end
end
