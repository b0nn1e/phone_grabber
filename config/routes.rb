Rails.application.routes.draw do
  root 'welcome#index'
  get 'models' => 'welcome#models'
  get 'model' => 'welcome#model'
  post 'search' => 'welcome#search'
end
