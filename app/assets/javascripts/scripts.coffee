$(document).on 'ready page:load', ->
  $(document).on 'change', '#j-brands', ->
    val = $(this).val()
    $('#j-select_model_wrrapper').addClass 'hide'
    $('#j-model_info_wrrapper').addClass 'hide'
    callback = (response) ->
      $('#j-select_model_wrrapper').removeClass('hide');
      $('#j-models').html('')
      $('#j-models').append $('<option></option>')
      $.each response, (key, val) ->
        $('#j-models').append $('<option></option>').attr('value', val.uri).text(val.name)

    if val.length > 0
      $.get '/models', {uri: val}, callback, 'json'
    else
      $('#j-select_model_wrrapper').addClass('hide');
      $('#j-model_info_wrrapper').addClass('hide');


  $(document).on 'change', '#j-models', ->
    val = $(this).val()
    callback = (response) ->
      $('#j-results').html ''
      $.each response, (key, val) ->
        $('#j-results').append $('<div class="property"></div>').html(val.key + ': ' + val.value)

      $('#j-model_info_wrrapper').removeClass('hide');

    if val.length > 0
      $.get '/model', {uri: val}, callback, 'json'
    else
      $('#j-model_info_wrrapper').addClass('hide');

  $(document).on 'ajax:before', '#j-search_form', ->
    $('#j-search_results').html ''
    $('#j_search_subimt').prop('disabled', true)

  $(document).on 'ajax:complete', '#j-search_form', ->
    $('#j_search_subimt').prop('disabled', false)

  $(document).on 'ajax:success', '#j-search_form', (xhr, data, status) ->
    if data.length > 0
      $.each data, (key, val) ->
        $('#j-search_results').append $('<div class="search-result"></div>').html(val.name)
    else
      $('#j-search_results').html $('<p>Nothing to show</p>')




    