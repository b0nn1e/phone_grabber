require 'open-uri'
class PhoneGrabberInterface
  class << self
    def domain
      raise NotImplementedError, "subclass did not define #domain"
    end

    def brands_url
      raise NotImplementedError, "subclass did not define #brands_url"
    end

    def brands
      @brands ||= get_brands
    end

    def models brand_uri
      html = get_html(brand_uri)
      parse_models_list(html)
    end

    def model model_uri
      html = get_html(model_uri)
      parse_model_info(html)
    end

    def find value
      search_words = prepare_string(value)
      return [] if search_words.empty?
      found_brands = detect_brands(search_words) || brands
      found_models = found_brands.map {|brand| models(brand[:uri]) }.flatten
      found_models.select do |model|
        search_words.any? { |word| model[:name].downcase.include?(word) }
      end
    end

    private

    def detect_brands search_words
      detect_brands_by_name(search_words) || detect_brands_by_chars(search_words)
    end

    def detect_brands_by_name search_words
      result = brands.select do |brand|
        brand_words = prepare_string(brand[:name])
        (brand_words & search_words).present?
      end
      result.present? ? result : nil
    end

    def detect_brands_by_chars search_words
      result = []
      search_words.each do |word|
        search_chars = arrs_to_words(word)
        chars_len = search_chars.count
        next if chars_len < 2
        brands.each do |brand|
          brand_words = prepare_string(brand[:name])
          brand_chars = arrs_to_words(brand_words)
          matches = (brand_chars & search_chars).count
          if matches == chars_len-1 || matches == chars_len
            result << brand
          end
        end
      end
      result.uniq!
      result.present? ? result : nil
    end

    def arrs_to_words arrs
      res = []
      [*arrs].each do |arr|
        arr.split('').each { |char| res << char }
      end
      res - [' ']
    end

    def prepare_string str
      str.downcase.scan(/[[:word:]]+/)
    end

    def get_brands
      html = open(brands_url).read
      parse_brands_list(html)
    end

    def get_html uri
      page_html = get_page_html(uri)
      other_pages = pagination_links(page_html)
      if other_pages.present?
        other_pages.each do |page_uri|
          page_html+= get_page_html(page_uri)
        end
      end
      page_html
    end

    def get_page_html uri
      open("#{domain}#{uri}").read
    end

    def parse_brands_list html
      raise NotImplementedError, "subclass did not define #parse_brands_list"
    end

    def parse_models_list html
      raise NotImplementedError, "subclass did not define #parse_models_list"
    end

    def pagination_links html
      raise NotImplementedError, "subclass did not define #pagination_links"
    end

    def parse_model_info html
      raise NotImplementedError, "subclass did not define #parse_model_info"
    end
  end
end