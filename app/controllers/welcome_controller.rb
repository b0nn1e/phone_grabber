class WelcomeController < ApplicationController

  def index
    @brands = Gsmarena.brands
  end

  def models
    render json: Gsmarena.models(params[:uri]).sort_by { |m| m["name"] }
  end

  def model
    render json: Gsmarena.model(params[:uri])
  end

  def search
    render json: Gsmarena.find(params[:value])
  end
end
