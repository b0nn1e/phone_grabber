class Gsmarena < PhoneGrabberInterface

  def self.domain
    'http://www.gsmarena.com/'
  end

  def self.brands_url
    # http://www.gsmarena.com/
    "#{Rails.root}/spec/support/fixtures/gsmarena-index.html"
  end

  def self.parse_brands_list html
    brands = []
    doc = Nokogiri::HTML(html)
    doc.css('.brandmenu-v2 ul li a').map do |showing|
      uri = showing["href"]
      name = showing.content
      brands << { uri: uri, name: name } if name.present?
    end
    brands
  end

  def self.parse_models_list html
    doc = Nokogiri::HTML(html)
    doc.css('.makers ul li').map do |showing|
      uri = showing.css('a').first["href"]
      name = showing.css('strong').first.content
      { uri: uri, name: name }
    end
  end

  #todo test it
  def self.parse_model_info html
    info = []
    doc = Nokogiri::HTML(html)
    doc.css('.main-review').each do |review|
      info << { key: 'Model name', value: review.css('h1.specs-phone-name-title').first.content }
      properties = review.css('.specs-spotlight-features li:first span')
      info << { key: 'Released', value: properties[0].content }
      info << { key: 'Weight', value: properties[1].content }
      info << { key: 'OS', value: properties[2].content }
      info << { key: 'Storage', value: properties[3].content }
      info << { key: 'Display', value: review.css('.help-display').first.content.squish }
      info << { key: 'Camera', value: review.css('.help-camera').first.content.squish }
      info << { key: 'Expansion', value: review.css('.help-expansion').first.content.squish }
      info << { key: 'Battery', value: review.css('.help-battery').first.content.squish }
      full_descr = ''
      review.css('#specs-list table').each do |table|
        full_descr += table.to_s
      end
      info << { key: "Properties", value: ActionController::Base.helpers.strip_links(full_descr) }
    end
    info
  end

  #todo test it
  def self.pagination_links html
    doc = Nokogiri::HTML(html)
    doc.css('.nav-pages a').map do |link|
      link['href']
    end
  end
end